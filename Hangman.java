import java.util.Scanner;

public class Hangman {

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
        System.out.println("Choose a word to start hangman!");
        String word = scanner.nextLine();
        runGame(word);
        scanner.close(); 
  }
  public static int isLetterInWord(String word, char c){
    c = toUpperCase(c);
    word = word.toUpperCase();
    for(int i = 0; i < word.length(); i++){
      if(word.charAt(i) == c){
        return i;
      }
    }
    return -1;
  }
  public static char toUpperCase(char c){
    return Character.toUpperCase(c);
  }
  public static void printWork(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3) {
      System.out.print("Your result is ");

      if (letter0) {
          System.out.print(word.charAt(0));
      } else {
          System.out.print("_");
      }

      if (letter1) {
          System.out.print(word.charAt(1));
      } else {
          System.out.print("_");
      }

      if (letter2) {
          System.out.print(word.charAt(2));
      } else {
          System.out.print("_");
      }

      if (letter3) {
          System.out.print(word.charAt(3));
      } else {
          System.out.print("_");
      }

      System.out.println();
  }


  public static void runGame(String word){

    int wrongGuesses = 0;
    boolean isChar0Revealed = false; 
    boolean isChar1Revealed = false; 
    boolean isChar2Revealed = false; 
    boolean isChar3Revealed = false; 


    while (wrongGuesses < 6 && (!isChar0Revealed || !isChar1Revealed || !isChar2Revealed || !isChar3Revealed)) {

      Scanner scanner = new Scanner(System.in);

      System.out.println("guess a letter 😈");
            char guess = scanner.nextLine().charAt(0);

            int letterPosition = isLetterInWord(word, guess);

      if (letterPosition == 0) {
                isChar0Revealed = true; 
            } else if (letterPosition == 1) {
                isChar1Revealed = true; 
            } else if (letterPosition == 2) {
                isChar2Revealed = true; 
            } else if (letterPosition == 3) {
                isChar3Revealed = true; 
            } else {
                wrongGuesses++;
            }

      printWork(word, isChar0Revealed, isChar1Revealed, isChar2Revealed, isChar3Revealed); 
    }
     if( wrongGuesses > 5 ){
       System.out.println("you lost stupid! The correct word was " + word);
    } else if (isChar0Revealed && isChar1Revealed && isChar2Revealed && isChar3Revealed) {
      System.out.println("CONGRATULATIONSSSS!! YOU 🫵 WIN!");	
    }
  }
}